<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array Output</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Output Array</h2>
        <div class="output">
            <?php
                $arrWarna = array("Blue", "Black", "Red", "Yellow", "Green");
                $arrNilai = array("Fuad" => 80, "ulin" => 90, "Fadil" => 75, "Fatan" => 85);

                echo "<pre>";
                print_r($arrWarna);
                echo "<br>";
                print_r($arrNilai);
                echo "</pre>";
            ?>
        </div>
    </div>
</body>
</html>
