<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nilai Siswa</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Nilai Siswa</h2>
        <div class="output">
            <?php
                $arrNilai = array("UPI" => 800, "DIMAS" => 900, "DAPA" => 750, "DAPI" => 850);
                echo "<div class='item'>Fuad: " . $arrNilai['UPI'] . "</div>"; 
                echo "<div class='item'>Alan: " . $arrNilai['DIMAS'] . "</div><br>"; 

                $arrNilai = array();
                $arrNilai['Amin'] = 800;
                $arrNilai['Aman'] = 950;
                $arrNilai['Amen'] = 770;
                echo "<div class='item'>Aman: " . $arrNilai['Aman'] . "</div>"; 
                echo "<div class='item'>Amin: " . $arrNilai['Amin'] . "</div>"; 
            ?>
        </div>
    </div>
</body>
</html>
