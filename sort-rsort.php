<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sorting Array dengan PHP</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <?php
            $arrNilai = array("UPI" => 80, "DIMAS" => 90, "DAPA" => 75, "DAPI" => 85);
            
            echo "<h2>Array sebelum diurutkan</h2>";
            echo "<pre>";
            print_r($arrNilai);
            echo "</pre>";

            sort($arrNilai);
            reset($arrNilai);
            echo "<h2>Array setelah diurutkan dengan sort()</h2>";
            echo "<pre>";
            print_r($arrNilai);
            echo "</pre>";

            rsort($arrNilai);
            reset($arrNilai);
            echo "<h2>Array setelah diurutkan dengan rsort()</h2>";
            echo "<pre>";
            print_r($arrNilai);
            echo "</pre>";
        ?>
    </div>
</body>
</html>
