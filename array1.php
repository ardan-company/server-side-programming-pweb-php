<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="styles.css">
    <title>Array Output</title>
</head>
<body>
    <div class="container">
        <h2>Array Output with PHP</h2>
        <div class="output">
            <?php
                $arrBuah = array("Mangga", "Apel", "Pisang", "Jeruk");
                echo "<p class='item'>Buah pertama: " . $arrBuah[0] . "</p>";
                echo "<p class='item'>Buah terakhir: " . $arrBuah[3] . "</p>"; 

                $arrWarna = array();
                $arrWarna[] = "Merah";
                $arrWarna[] = "Biru";
                $arrWarna[] = "Hijau";
                $arrWarna[] = "Putih";
                echo "<p class='item'>Warna pertama: " . $arrWarna[0] . "</p>"; 
                echo "<p class='item'>Warna ketiga: " . $arrWarna[2] . "</p>"; 
            ?>
        </div>
    </div>
</body>
</html>
