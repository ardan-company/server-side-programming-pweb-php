<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cetak Angka Ganjil</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Angka Ganjil dari 0 hingga 99</h2>
        <div class="output">
            <?php
                function cetak_ganjil(){
                    $output = "";
                    for ($i=0; $i < 100; $i++) {
                        if ($i % 2 == 1) {
                            $output .= "$i, ";
                        }
                    }
                    return $output;
                }
                echo cetak_ganjil();
            ?>
        </div>
    </div>
</body>
</html>
