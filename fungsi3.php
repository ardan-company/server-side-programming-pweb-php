<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Luas Persegi</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <?php
            function persegi($sisi){
                return $sisi * $sisi;
            }
            
            $r = 5;
            $luas = persegi($r);
        ?>
        <h1>Luas Persegi dengan Panjang Sisi <?php echo $r; ?> adalah:</h1>
        <div class="result">
            <?php echo $luas; ?>
        </div>
    </div>
</body>
</html>
