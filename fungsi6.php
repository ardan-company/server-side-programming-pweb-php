<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Fungsi PHP dan Daftar Fungsi Terdefinisi</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <?php
            function luas_lingkaran($jari){
                return 3.14 * $jari * $jari;
            }

            $arr = get_defined_functions();
            echo "<h2>Daftar Fungsi PHP Terdefinisi</h2>";
            echo "<pre>";
            print_r($arr);
            echo "</pre>";

            $jari = 7;
            $luas = luas_lingkaran($jari);
            echo "<h2>Luas Lingkaran dengan Jari-jari $jari adalah:</h2>";
            echo "<p class='luas'>$luas</p>";
        ?>
    </div>
</body>
</html>
