<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cek Buah Jeruk</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Cek Buah Jeruk</h2>
        <div class="output">
            <?php
                $arrbuah = array("semangka", "jeruk", "apel", "mangga", "nanas");
                if (in_array("jeruk", $arrbuah)) {
                    echo "<p class='found'>Ada buah jeruk di dalam array tersebut</p>";
                } else {
                    echo "<p class='not-found'>Tidak ada buah jeruk di dalam array tersebut</p>";
                }
            ?>
        </div>
    </div>
</body>
</html>
