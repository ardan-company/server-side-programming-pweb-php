<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Warna Pelangi</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Warna Pelangi</h2>
        <div class="output">
            <?php
                $arrWarna = array("Red", "black", "Yellow", "Green", "Blue", "Purple");

                echo "<div class='section-title'>Menampilkan isi array dengan FOR:</div>";
                for ($i = 0; $i < count($arrWarna); $i++) {
                    echo "<div class='item' style='color: $arrWarna[$i]'>Warna pelangi: $arrWarna[$i]</div>";
                }

                echo "<br><div class='section-title'>Menampilkan isi array dengan FOREACH:</div>";
                foreach ($arrWarna as $warna) {
                    echo "<div class='item' style='color: $warna'>Warna pelangi: $warna</div>";
                }
            ?>
        </div>
    </div>
</body>
</html>
