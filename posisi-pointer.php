<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Manipulasi Array dengan Pointer</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <?php
            $transport = array('mobil', 'sepeda', 'motor', 'Pesawat');
            
            echo "<h2>Array Transportasi</h2>";
            echo "<pre>";
            print_r($transport);
            echo "</pre>";

            $mode = current($transport);
            echo "<p>Current: $mode</p>";

            $mode = next($transport);
            echo "<p>Next: $mode</p>";

            $mode = current($transport);
            echo "<p>Current setelah next: $mode</p>";

            $mode = prev($transport);
            echo "<p>Prev: $mode</p>";

            $mode = end($transport);
            echo "<p>End: $mode</p>";

            $mode = current($transport);
            echo "<p>Current setelah end: $mode</p>";
        ?>
    </div>
</body>
</html>
