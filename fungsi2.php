<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tampilan Bilangan Ganjil</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <?php
            function cetak_ganjil($awal, $akhir){
                $output = "";
                for ($i = $awal; $i <= $akhir; $i++) {
                    if ($i % 2 == 1) {
                        $output .= "$i, ";
                    }
                }
                return $output;
            }
            
            $a = 10;
            $b = 50;
            echo "<h1>Bilangan ganjil dari $a sampai $b, adalah:</h1>";
            echo "<p class='ganjil'>" . cetak_ganjil($a, $b) . "</p>";
        ?>
    </div>
</body>
</html>
