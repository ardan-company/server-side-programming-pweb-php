<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Modifikasi String</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <?php
            function tambah_string($str){
                $str = $str . ", Bantul, Yogyakarta";
                return $str;
            }

            $string = "Universitas Ahmad Dahlan";
            echo "<p>\$string = $string</p>";
            echo "<p>" . tambah_string($string) . "</p>";
            echo "<p>\$string = $string</p>";
        ?>
    </div>
</body>
</html>
