<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Nilai Siswa</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Nilai Siswa</h2>
        <div class="output">
            <div class="section-title">Menampilkan isi array asosiatif dengan foreach:</div>
            <?php
                $arrNilai = array("UPI" => 80, "DIMAS" => 90, "DAPA" => 75, "DAPI" => 85);

                foreach ($arrNilai as $nama => $nilai) {
                    echo "<div class='item'>Nilai $nama = $nilai</div>";
                }

                reset($arrNilai);
                echo "<br><div class='section-title'>Menampilkan isi array asosiatif dengan WHILE dan LIST:</div>";

                $keys = array_keys($arrNilai);
                while ($keys) {
                    $nama = array_shift($keys);
                    $nilai = $arrNilai[$nama];
                    echo "<div class='item'>Nilai $nama = $nilai</div>";
                }
            ?>
        </div>
    </div>
</body>
</html>
