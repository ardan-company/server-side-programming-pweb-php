<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Array Sorting</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <div class="container">
        <h2>Array Sorting</h2>
        <div class="output">
            <?php
                $arrNilai = array("UPI" => 80, "DIMAS" => 90, "DAPA" => 75, "DAPI" => 85);
                echo "<b>Array sebelum diurutkan</b><br>";
                echo "<pre class='array-output'>";
                print_r($arrNilai);
                echo "</pre>";

                asort($arrNilai);
                reset($arrNilai);
                echo "<b>Array setelah diurutkan dengan asort()</b><br>";
                echo "<pre class='array-output'>";
                print_r($arrNilai);
                echo "</pre>";

                arsort($arrNilai);
                reset($arrNilai);
                echo "<b>Array setelah diurutkan dengan arsort()</b><br>";
                echo "<pre class='array-output'>";
                print_r($arrNilai);
                echo "</pre>";
            ?>
        </div>
    </div>
</body>
</html>
